---
author: "Heng Ye"
title: "First post"
date: "2022-09-17"
description: "My first post on a GitLab pages"
---

Hello from my new GitLab site! <!--more-->

I have just moved my website from GitHub to GitLab. I prefer to work with the GitLab ecosystem, especially
its CI system, more that GitHub. Additionally, GitLab probably will not start things like Copilot which
might take open source software and not consider the license. From a first perspective, however, GitHub Pages
is much easier to use than GitLab pages at this time. I think GitLab should consider creating a tool to set properties
of static site generators such as themes, but that would require isolating one generator as being officially supported
and reduce the others to being second class citizens which might not be such a good idea. Still, the amount of setup
required for GitLab is a lot if, for example, a marketing person was trying to set up the website. Also, having to 
remove a lot of Lorem Ipsum from the project template isn't quite satisfying as starting from a blank editor buffer.

