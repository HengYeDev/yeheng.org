# Heng Ye's Site

Hello! 

## My software projects

* [UBports Ubuntu Touch port to Sony Xperia 10 (kirin)](https://forums.ubports.com/topic/5452/sony-xperia-10-kirin)
* [Sailfish x86 Linux Distribution for x86_64 Tablets and Laptops](https://sailfish-x86.yeheng.org)
* [Sailtrix, a matrix app with E2EE for Sailfish](https://openrepos.net/content/hengyedev/sailtrix)
* [Sailfish OS on Samsung Galaxy Tab S6 Lite](https://forum.sailfishos.org/t/sailfishos-on-samsung-galaxy-tab-s6-lite-wi-fi-gta4xlwifi/10767)
* [PDF Stitcher](https://pdf.yeheng.org)
* [PDF Merger](https://pdfmerge.yeheng.org)





